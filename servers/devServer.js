const express = require("express");
const fetch = require("node-fetch");
const multer = require("multer");
const formidable = require("formidable");
const path = require("path");
const { bgGreen, white, whiteBright, bgRed } = require("chalk");

const app = express();
const PORT = 9001;
const log = console.log;
const upload = multer({ storage: multer.memoryStorage() });

app.use(express.static(path.resolve(__dirname, "..", "dist")));
app.set("view engine", "ejs");
app.set("views", path.resolve(__dirname, "..", "dist"));

app.get("*", function devExpressDefaultGETCallback(req, res) {
    res.render("index.ejs");
});

app.post("/api/imageUpload", function(req, res) {
    const { files = [] } = req;
    //const form = formidable({ keepExtensions: true, multiples: true });
    var form = new formidable.IncomingForm();
    form.multiples = true;

    form.parse(req, (err, fields, files) => {
        if (err) {
            next(err);
            res.send(
                Promise.reject({
                    status: "error",
                    message: "formidable parsed as error"
                })
            );
        }

        // use the endpoint, if you face CORS issue
        // const endpoint = "https://cors-anywhere.herokuapp.com/https://pictshare.net/api/upload.php";

        const endpoint = "https://pictshare.net/api/upload.php";
        return fetch(endpoint, {
            method: "POST",
            body: files
        })
            .then(response => {
                return response.json();
            })
            .then(res => {
                res.send(res);
            })
            .catch(error => res.send(error));
    });
});

app.listen(PORT, function devExpressListenCallback(error) {
    if (error) {
        log(white.bgRed("Couldn't START DEVELOPMENT server"));
    } else {
        log(whiteBright.bgGreen("DEVELOPMENT SERVER started successfully!..."));
        log(
            whiteBright.bgGreen(
                "Open following url in browser: http://localhost:" + PORT + "/"
            )
        );
    }
});
