import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Loader from "../src/shared/components/presentational/Loader";
import ImageUploadMethodOne from "../src/pages/image-upload-method-one/ImageUploadMethodOne";
const AppRoutes = () => {
    return (
        <BrowserRouter>
            <div className="app-container">
                <Loader />
                <div className="app-body-container">
                    <Switch>
                        <Route path="/" component={ImageUploadMethodOne} />
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    );
};

export default AppRoutes;
