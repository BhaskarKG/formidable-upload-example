import webpack from "webpack";
import { white, whiteBright, bgRedBright, bgGreenBright } from "chalk";
import commonConfig from "./configs/webpack/common";
import devConfig from "./configs/webpack/dev";
import prodConfig from "./configs/webpack/prod";

const log = console.log;
const {
    WEBPACK_BUILD_ENVIRONMENT = "development",
    API_BASE_URL = "",
    API_ENVIRONMENT = "UAT",
    API_REQUEST_TIMEOUT = 3000,
    NODE_REQUEST_TIMEOUT = 5000
} = process.env;
let config = {};
let webpackModeConfig =
    WEBPACK_BUILD_ENVIRONMENT === "development" ? devConfig : prodConfig;

if (WEBPACK_BUILD_ENVIRONMENT === "development") {
    log(whiteBright.bgGreenBright("WEBPACK MODE: development"));
} else if (WEBPACK_BUILD_ENVIRONMENT === "production") {
    log(whiteBright.bgGreenBright("WEBPACK MODE: production"));
} else {
    log(whiteBright.bgRedBright("WEBPACK MODE: unknown"));
}

config = {
    ...commonConfig,
    ...webpackModeConfig,
    plugins: [
        ...(commonConfig.plugins ? commonConfig.plugins : []),
        ...(webpackModeConfig.plugins ? webpackModeConfig.plugins : []),
        new webpack.DefinePlugin({
            WEBPACK_BUILD_ENVIRONMENT: JSON.stringify(
                WEBPACK_BUILD_ENVIRONMENT
            ),
            API_BASE_URL: JSON.stringify(API_BASE_URL),
            API_ENVIRONMENT: JSON.stringify(API_ENVIRONMENT),
            API_REQUEST_TIMEOUT,
            NODE_REQUEST_TIMEOUT
        })
    ]
};
export default config;
