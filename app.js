import React from "react";
import { render } from "react-dom";
import regeneratorRuntime from "regenerator-runtime";
import ReactReduxConnector from "./store/ReactReduxConnector";
import apis from "./configs/apis";
import "./src/scss/app.scss";

apis.setEndpoints();

render(<ReactReduxConnector />, document.getElementById("app"));
