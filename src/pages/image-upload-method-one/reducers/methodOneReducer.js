const initialAppState = {
    wrongItemsDeliveredScreenshots: []
};

export function methodOneReducer(state = initialAppState, action) {
    const { type, data } = action;
    switch (type) {
        case "UPDATE_RETURN_ITEM_SCREENSHOTS":
            return {
                wrongItemsDeliveredScreenshots: [...data]
            };
        case "RESET_RETURN_ITEM_SCREENSHOTS_STATE":
            return {
                wrongItemsDeliveredScreenshots: []
            };
        default:
            return state;
    }
}
