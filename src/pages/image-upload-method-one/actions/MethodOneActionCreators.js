export function dispatchUpdateReturnItemsScreenshotsAction(itemObject, stateObject) {
    let data = [];
    const itemsFromReduxStore = [...stateObject];
    const itemIndex = itemsFromReduxStore.findIndex(item => item.itemCode === itemObject.itemCode);
    if(itemIndex >= 0) {
        itemsFromReduxStore.splice(itemIndex, 1);
    }
    data = [
        ...itemsFromReduxStore,
        {...itemObject}
    ]
    return {
        type: "UPDATE_RETURN_ITEM_SCREENSHOTS",
        data
    };
}

export function dispatchResetReturnItemsScreenshotsStateAction() {
    return {
        type: "RESET_RETURN_ITEM_SCREENSHOTS_STATE"
    };
}

export function clientToNodeImageUploadRequestAction(formData) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            // const endpoint = "https://cors-anywhere.herokuapp.com/https://pictshare.net/api/upload.php";
            const endpoint = "/api/imageUpload";
            return fetch(
                endpoint,
                {
                    method: "POST",
                    body: formData
                }
            ).then((response) => {
                return response.json()
            }).then((json) => {
                resolve(json);
            }).catch((error) => {
                reject(error);
            });
        });
    }
}
