import React from "react";

const ImagePreviewContainer = props => {
    const { convertBase64ToPreviewUrl, wrongItemDeliveredScreenshots: {screenshots = []} = {} } = props;

    if (screenshots.length === 0) return null;

    return (
        <div className="preview-container">
            {screenshots.map((image) => {
                const {id, filename, type, base64} = image;
                return (
                    <div key={id} className="preview-card">
                        <div className="image-placeholder">
                            <img
                                alt={filename}
                                src={convertBase64ToPreviewUrl(
                                    type,
                                    base64
                                )}
                            />
                        </div>
                    </div>
                );
            })}
        </div>
    );
};

export default ImagePreviewContainer;
