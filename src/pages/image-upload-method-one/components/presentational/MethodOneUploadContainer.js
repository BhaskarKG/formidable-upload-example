import React from "react";

const ImageUploadContainer = props => {
    const {
        onChangeOfReturnItemScreenshotUploadField,
        onClickOfReturnItemButton
    } = props;

    return (
        <div className="upload-container">
            <input
                type="file"
                id="image-upload-field"
                onChange={e => onChangeOfReturnItemScreenshotUploadField(e)}
                multiple
            />
            <input
                className="upload-images-button"
                type="button"
                value="SUBMIT"
                onClick={e => onClickOfReturnItemButton(e)}
            />
        </div>
    );
};

export default ImageUploadContainer;
