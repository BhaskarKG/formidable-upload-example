import React, { Component } from "react";
import { connect } from "react-redux";
import "whatwg-fetch";
import {
    showLoader,
    hideLoader
} from "../../shared/actions/loaderActionCreators";
import { dispatchUpdateReturnItemsScreenshotsAction, clientToNodeImageUploadRequestAction } from "./actions/MethodOneActionCreators";
import MethodOneUploadContainer from "./components/presentational/MethodOneUploadContainer";
import MethodOnePreviewContainer from "./components/presentational/MethodOnePreviewContainer";
import "./scss/image-upload-method-one.scss";

class ImageUploadMethodOne extends Component {
    constructor(props) {
        super(props);
        this.state = {
            disableSubmitButton: true,
            wrongItemDeliveredScreenshots: this.props.itemScreenshotsFromReduxStore
        };
    }

    getNewWrongItemDeliveredScreenshotsObject = (filename, type, base64, file) => {
        return {
            id: new Date().getTime(),
            filename,
            type,
            base64,
            file
        };
    }

    getImageExtension = (img = "") => {
        let dotIndex = img.lastIndexOf(".");

        if (dotIndex === -1) return "";
        return img.substr(++dotIndex);
    };

    convertImageToBase64 = (type, base64Object) => {
        const ptr = new RegExp(`^(data:${type};base64,)`, "ig");
        return encodeURIComponent(base64Object.replace(ptr, ""));
    };

    convertBase64ToImage = (b64Data, contentType = "", sliceSize = 512) => {
        const byteCharacters = atob(decodeURIComponent(b64Data));
        let byteArrays = [];

        for (
            let offset = 0;
            offset < byteCharacters.length;
            offset += sliceSize
        ) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);

            let byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };

    convertBase64ToPreviewUrl = (type, base64) => {
        const prefix = `data:${type};base64,`;
        return prefix + decodeURIComponent(base64);
    };

    // appendUserUploadedImage = (
    //     component,
    //     fileReaderObject,
    //     userUploadedImagesCount,
    //     currentImageObject
    // ) => {
    //     const { name, type } = currentImageObject;

    //     fileReaderObject.onloadend = () => {
    //         const image = {
    //             fileName: name,
    //             mimeType: type,
    //             base64: component.convertImageToBase64(
    //                 type,
    //                 fileReaderObject.result
    //             )
    //         };

    //         component.setState(
    //             prevState => ({
    //                 ...prevState,
    //                 wrongItemDeliveredScreenshots: {
    //                     ...prevState.wrongItemDeliveredScreenshots,
    //                     screenshots = []
    //                 }
    //             }),
    //             () => {
    //                 if (
    //                     component.state.userUploadedImagesList.length ==
    //                     userUploadedImagesCount
    //                 ) {
    //                     component.props.hideLoader();
    //                 }
    //             }
    //         );
    //     };

    //     fileReaderObject.readAsDataURL(currentImageObject);
    // };

    clientToNodeImageUploadRequest = async(formData) => {
        this.props.showLoader();
        try {
            const response = await this.props.clientToNodeImageUploadRequestAction(formData);
            const res = await response.json();
            console.log("Image Uploaded with success response: ", res);
        } catch(error) {
            console.log("Image Upload Error response: ", error);
        }

    }

    requestForReturnItemScreenshotsUpload = async(data) => {
        const endpoint = "https://cors-anywhere.herokuapp.com/https://pictshare.net/api/upload.php";
        try {
            const response = await fetch(endpoint, {
                method: "POST",
                body: data
            });
            const json = await response.json();
            console.log("Upload Image API Success with: ", json);
            this.props.hideLoader();
            return true;
        } catch (e) {
            console.log("Upload Image API Error with: ", e.message);
            this.props.hideLoader();
            return false;
        }
    }

    uploadItemScreenshots = async wrongItemDeliveredScreenshots => {
        // convert base64 to file object
        const {screenshots = []} = wrongItemDeliveredScreenshots;
        const formData = new FormData();
        screenshots.forEach((screenshot) => {
            const { filename, type, base64, file } = screenshot;
            formData.append(
                "file",
                file,
                filename
            );
        });
        // below API request method is used to request from CLIENT to ENDPOINT SERVER DIRECTLY
        //const imageUploadStatus = await this.requestForReturnItemScreenshotsUpload(formData);

        // Below API request method is used to request from CLIENT TO NODE and then to ENDPOINT SERVER
        const imageUploadStatus = await this.clientToNodeImageUploadRequest(formData);
        if(imageUploadStatus === true) {
            console.log("Images uploaded successfully...");
        } else if(imageUploadStatus === false) {
            console.log("Image upload request failed");
        } else {
            console.log("Fetch API response: ", imageUploadStatus);
        }
    };

    onChangeOfReturnItemScreenshotUploadField = e => {
        const {
            wrongItemDeliveredScreenshots = {},
            wrongItemDeliveredScreenshots: {
                screenshots: {
                    length: stateScreenshotsCount = 0
                } = []
            } = {}
        } = this.state;
        const { files = [], length: filesCount = 0 } = e.target;
        const uploadedScreenshots = [];
        const IMAGE_SIZE_LIMIT_IN_BYTES = 1000000;
        const IMAGE_EXTENSIONS_ALLOWED = ["png", "jpeg", "jpg", "bmp"];
        const result = {
            status: true,
            message: ""
        };

        this.props.showLoader();

        if (!files || files.length === 0) return false;

        if(stateScreenshotsCount >= 2) {
            result.status = false;
            result.message = "Max two files are allowed for upload. You already selected two images for upload";
        } else if(filesCount > 2) {
            result.status = false;
            result.message = "Max two files are allowed for upload.";
        } else {
            for (let i = 0; i < files.length; i++) {
                const { name, type, size } = files[i];
                const extension = this.getImageExtension(files[i].name);

                if (extension.trim().length === 0) {
                    result.status = false;
                    result.message = `Incorrect file - <strong>${name}</strong> uploaded. File with extensions - .png, .jpeg, .jpg & .bmp are allowed.`;
                    break;
                }

                if (
                    IMAGE_EXTENSIONS_ALLOWED.indexOf(
                        extension.trim().toLowerCase()
                    ) === -1
                ) {
                    result.status = false;
                    result.message = `Incorrect image - <strong>${name}</strong> uploaded. Only file with extensions - .png, .jpeg, .jpg & .bmp are allowed.`;
                    break;
                }

                if (size > IMAGE_SIZE_LIMIT_IN_BYTES) {
                    result.status = false;
                    result.message = `Image - <strong>${name}</strong> size limit exceeded. Max 1MB size allowed for upload.`;
                    break;
                }
                const screenshotObject = this.getNewWrongItemDeliveredScreenshotsObject(
                    name,
                    type,
                    "",
                    files[i]
                );
                uploadedScreenshots.push(screenshotObject);
            }
        }

        if (!result.status) {
            console.log("Error: ", result.message);
            this.setState({disableSubmitButton: true});
            this.props.hideLoader();
            return;
        } else {
            //const fileReaderObject = new FileReader();
            console.log("Valid image files are uploaded...");
            this.setState({
                disableSubmitButton: false,
                wrongItemDeliveredScreenshots: {
                    ...wrongItemDeliveredScreenshots,
                    screenshots: [
                        ...wrongItemDeliveredScreenshots.screenshots,
                        ...uploadedScreenshots
                    ]
                }
            });
            this.props.hideLoader();
            return;
            // for (let i = 0; i < images.length; i++) {
            //     (function(
            //         component,
            //         fileReaderObject,
            //         userUploadedImagesCount,
            //         currentImageObject
            //     ) {
            //         component.appendUserUploadedImage(
            //             component,
            //             fileReaderObject,
            //             userUploadedImagesCount,
            //             currentImageObject
            //         );
            //     })(this, fileReaderObject, images.length, files[i]);
            // }
        }
    };

    onClickOfReturnItemButton = e => {
        e.preventDefault();
        debugger;
        this.props.showLoader();
        const { wrongItemDeliveredScreenshots = [] } = this.state;
        const {itemsScreenshotsListFromReduxStore = []} = this.props;
        this.props.dispatchUpdateReturnItemsScreenshotsAction(wrongItemDeliveredScreenshots, itemsScreenshotsListFromReduxStore);
        this.uploadItemScreenshots(wrongItemDeliveredScreenshots);
    };

    render() {
        const {
            onClickOfReturnItemButton,
            onChangeOfReturnItemScreenshotUploadField,
            convertBase64ToPreviewUrl,
        } = this;
        const {
            wrongItemDeliveredScreenshots
        } = this.state;
        return (
            <div className="image-preview-and-upload-container">
                <MethodOneUploadContainer
                    onClickOfReturnItemButton={onClickOfReturnItemButton}
                    onChangeOfReturnItemScreenshotUploadField={
                        onChangeOfReturnItemScreenshotUploadField
                    }
                />
                <MethodOnePreviewContainer
                    convertBase64ToPreviewUrl={convertBase64ToPreviewUrl}
                    wrongItemDeliveredScreenshots={wrongItemDeliveredScreenshots}
                />
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const { imageUploadMethodOne: { wrongItemsDeliveredScreenshots = [] } = {} } = state;
    const {code = "FNL36363636"} = ownProps;
    const associatedScreenshotsObject = wrongItemsDeliveredScreenshots.filter((itemObject) => itemObject.code === ownProps.code);
    const itemScreenshotsFromReduxStore = associatedScreenshotsObject.length >= 1? {...associatedScreenshotsObject[0]} : { itemCode: code, screenshots: [], imageDetails: [] };
    return {
        itemsScreenshotsListFromReduxStore: [...wrongItemsDeliveredScreenshots],
        itemScreenshotsFromReduxStore
    };
}

function mapDispatchToProps(dispatch, ownProps) {
    return {
        showLoader: () => dispatch(showLoader()),
        hideLoader: () => dispatch(hideLoader()),
        dispatchUpdateReturnItemsScreenshotsAction: (itemObject, stateObject) =>
            dispatch(dispatchUpdateReturnItemsScreenshotsAction(itemObject, stateObject)),
        clientToNodeImageUploadRequestAction: (formData) =>
            dispatch(clientToNodeImageUploadRequestAction(formData))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ImageUploadMethodOne);
