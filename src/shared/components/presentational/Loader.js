import React, { Component } from "react";
import { connect } from "react-redux";

class Loader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { showLoader } = this.props;
        if (showLoader)
            return (
                <div className="loader-wrapper">
                    <div className="ripple-loader">
                        <div></div>
                        <div></div>
                    </div>
                </div>
            );

        return null;
    }
}

function mapStateToProps(state, ownProps = {}) {
    const { loader: { showLoader = false } = {} } = state;
    return {
        showLoader
    };
}

export default connect(mapStateToProps)(Loader);
