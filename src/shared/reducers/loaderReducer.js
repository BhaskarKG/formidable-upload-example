const initialLoaderState = {
    showLoader: false
};

export function loaderReducer(state = initialLoaderState, action) {
    switch (action.type) {
        case "SHOW_LOADER":
            return { showLoader: true };
        case "HIDE_LOADER":
            return { showLoader: false };
        default:
            return state;
    }
}
