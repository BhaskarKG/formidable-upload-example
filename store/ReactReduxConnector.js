import React from "react";
import { Provider } from "react-redux";
import AppRoutes from "../routes/AppRoutes";
import { appStore } from "./store";

const ReactReduxConnector = () => {
    const store = appStore();
    return (
        <Provider store={store}>
            <AppRoutes />
        </Provider>
    );
};

export default ReactReduxConnector;
