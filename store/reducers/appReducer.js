import { combineReducers } from "redux";
import { loaderReducer } from "../../src/shared/reducers/loaderReducer";
import {methodOneReducer} from "../../src/pages/image-upload-method-one/reducers/methodOneReducer";
export const appReducers = () => {
    return combineReducers({
        loader: loaderReducer,
        imageUploadMethodOne: methodOneReducer
    });
};
