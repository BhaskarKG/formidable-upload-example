import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import { appReducers } from "./reducers/appReducer";

const buildMode = WEBPACK_BUILD_ENVIRONMENT || "";

const composeEnhancers =
    typeof window === "object" &&
    buildMode === "development" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
              // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
          })
        : compose;

const enhancer = composeEnhancers(
    applyMiddleware(reduxThunk)
    // other store enhancers if any
);

export function appStore() {
    return createStore(appReducers(), enhancer);
}
