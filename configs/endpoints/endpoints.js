export const endpoints = {
    allPosts: {
        path: "/posts",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    post: {
        path: "/posts/",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    commentByPostId: {
        path: "/posts/",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    allComments: {
        path: "/comments",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    allAlbums: {
        path: "/albums",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    allPhotos: {
        path: "/photos",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    allTodos: {
        path: "/todos",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    allUsers: {
        path: "/users",
        baseUrl: "http://jsonplaceholder.typicode.com"
    },
    bottsAvatar: {
        path: "/bottts/",
        baseUrl: "https://avatars.dicebear.com/v2"
    },
    pravatar: {
        path: "/300",
        baseUrl: "https://i.pravatar.cc"
    },
    tinyImage: {
        path: "/50",
        baseUrl: "https://via.placeholder.com"
    },
    smallImage: {
        path: "/100",
        baseUrl: "https://via.placeholder.com"
    },
    mediumImage: {
        path: "/150",
        baseUrl: "https://via.placeholder.com"
    },
    largeImage: {
        path: "/200",
        baseUrl: "https://via.placeholder.com"
    },
    zoomImage: {
        path: "/400",
        baseUrl: "https://via.placeholder.com"
    }
};
