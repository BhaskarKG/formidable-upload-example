export const ejsLoader = {
    test: /\.ejs/i,
    exclude: /node_modules/i,
    use: ["ejs-loader"]
};

export const jsLoader = {
    test: /\.js$/i,
    exclude: /node_modules/i,
    use: ["babel-loader"]
};

export const cssLoader = {
    test: /\.(c|sc)ss$/i,
    exclude: /node_modules/i,
    use: ["style-loader", "css-loader", "sass-loader"]
};

export const fontsLoader = {
    test: /\.((ttf|woff|woff2|eot|svg)(\?v=\d{1,}.\d{1,}.\d{1,})?)$/i,
    exclude: /node_modules/i,
    use: ["file-loader"]
};

export const imagesLoader = {
    test: /\.(png|svg|jpg|jpeg|gif)$/i,
    exclude: /node_modules/i,
    use: ["file-loader", "url-loader"]
};
