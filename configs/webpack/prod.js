import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import {
    ejsLoader,
    jsLoader,
    cssLoader,
    fontsLoader,
    imagesLoader
} from "./bundlers";

export default {
    mode: "production",
    module: {
        rules: [ejsLoader, jsLoader, cssLoader, fontsLoader, imagesLoader]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(
                __dirname,
                "..",
                "..",
                "src",
                "views",
                "index.ejs"
            ),
            filename: path.resolve(__dirname, "..", "..", "dist", "index.ejs")
        }),
        new CleanWebpackPlugin()
    ]
};
