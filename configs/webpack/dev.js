import path from "path";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import {
    ejsLoader,
    jsLoader,
    cssLoader,
    fontsLoader,
    imagesLoader
} from "./bundlers";

export default {
    mode: "development",
    devtool: "inline-source-map",
    module: {
        rules: [ejsLoader, jsLoader, cssLoader, fontsLoader, imagesLoader]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(
                __dirname,
                "..",
                "..",
                "src",
                "views",
                "index.ejs"
            ),
            filename: path.resolve(__dirname, "..", "..", "dist", "index.ejs")
        }),
        // new BundleAnalyzerPlugin(),
        new CleanWebpackPlugin()
    ]
};
