import path from "path";

export default {
    entry: path.resolve(__dirname, "..", "..", "app.js"),
    output: {
        path: path.resolve(__dirname, "..", "..", "dist"),
        filename: "bundle.js",
        publicPath: "/"
    }
};
