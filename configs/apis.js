import { endpoints } from "./endpoints/endpoints";

let apis = null;

function getEndpoints() {
    return { ...apis };
}

function getEndpoint(endpoint) {
    return apis[endpoint];
}

function setEndpoints() {
    apis = Object.keys(endpoints).reduce(function(acc, endpoint) {
        const { baseUrl = "", path = "" } = endpoints[endpoint];
        acc[endpoint] = `${baseUrl ? baseUrl : API_BASE_URL}${path}`;
        return acc;
    }, {});
}

export default {
    setEndpoints: setEndpoints,
    getEndpoint: getEndpoint,
    getEndpoints: getEndpoints
};
