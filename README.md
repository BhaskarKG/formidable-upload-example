# React Features

* NPM commands
  * `npm run buildDev` - to build bundle files
  * `npm run startDev` - to express server

* localhost -  `http://localhost:9001/`
* Added sample images for testing in - `/docs/images-for-testing`
* Test API used - `https://cors-anywhere.herokuapp.com/https://pictshare.net/api/upload.php`
* Test API with Proxy to avoid CORS issue - `https://cors-anywhere.herokuapp.com/https://pictshare.net/api/upload.php`
* Screenshot of Tested API result in postman: `/docs/screenshots/postman-api-screenshot.png`
* To test working Image upload feature from CLIENT to API ENDPOINT directly
  * Make following changes in /src/pages/image-upload-method-one/ImageUploadMethodOne.js
    * Uncomment Line No: 156
    * Comment Line No: 159

